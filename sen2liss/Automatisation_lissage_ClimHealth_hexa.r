################################################################################
########## ESTIMATION D'UN LISSAGE SUR LES DONNEES BRUTES ##########
################################################################################


##### Dans le cas ou on a plusieurs polygones, la periode estimee representera la date min et la date max ou on a des observations sur au moins un polygone #####

# Modules
library(lubridate)
library(caret)
library(fda)
library(fda.usc)
library(Metrics)
library(imputeTS)
library(gtools)
library(parallel)
library(aweek)


# Repertoire de travail
# setwd("C:/Users/UTILISATEUR/Desktop/ClimHealth")
setwd("/home/webdev/shiny/appli/sen2extract/www/extractions/yangoon/")

# Modification des couleurs par defaut
palette(c("black","red","green3","blue","cyan","magenta","yellow","gray"))

# Pourcentage de nodata (a etudier)
seuil.NA = 75

# Calcul du nombre de coeurs qu'on peut utiliser dans le serveur
nb.coeurs = detectCores()

### Creation d'une fonction sur la mise en forme des fichiers de chaque indice et decomposition en sous-jeux de donn�es par polygone ###
menf = function(indice){
  ### Importation du jeu de donnees et mise en forme des donnees ###
#  donnees = read.csv(paste("./Bases de donnees/Yangon_lepto_250m_WGS84_Sen2Extract_20210210155931/Yangon_lepto_250m_WGS84_",indice,"_2016-09-01_2020-12-31.csv",sep=""),stringsAsFactors=TRUE)
  donnees = read.csv(paste("./Yangon_hexasurv_named_",indice,"_current.csv",sep=""),stringsAsFactors=TRUE)

  # Definition du jeu d'origine avant extraction + fid
  jeu.orig = unique(donnees[,-match(c("date","tile","filename","count","nodata","nbcld","cldprb","min","max","mean","std","median","percentile_25","percentile_75"),names(donnees))])
  
  # On ne conserve que les variables d'extraction
  donnees = donnees[,match(c("date","fid","tile","filename","count","nodata","nbcld","cldprb","min","max","mean","std","median","percentile_25","percentile_75"),names(donnees))]
  
  # Creation d'une variable date en format date
  donnees$date2 = as.Date(donnees$date, "%Y-%m-%d")
  
  # Creation d'une variable part de NAs dans le buffer
  donnees$pct.na = round(donnees$nodata / (donnees$count + donnees$nodata) * 100,digits=2)
  
  # Modification des indices environnementaux pour etre pris entre -1 et 1
  for (i in c("min","max","mean","std","median","percentile_25","percentile_75")) donnees[,i] = round(donnees[,i]/10000,digits=4)

  ### Au prealable, definition des dates min et max pour realiser l'Imputation sur le lissage
  # Calcul pour avoir une estimation sur des annees completes (sur la meme periode pour tous les polygones)
  min.date = min(donnees$date2)   # A partir de la 1ere date ou on a des observations sur au moins un polygone
  #max.date.reelle = max(donnees$date2)   # jusqu'a la derniere date ou on a des observations sur au moins un polygone
  max.date.reelle = Sys.Date() # jusqu'a la date de l'execution du programme
  ## Modification de la derniere date d'observation pour avoir des cycles complets (annees completes)
  max.date = Sys.Date()
  if ((month(max.date) < month(min.date)) | (month(min.date) == month(max.date) & day(max.date) < day(min.date)))
    max.date = as.Date(paste(year(max.date.reelle),substr(min.date,6,10),sep="-")) else
      if ((month(max.date) > month(min.date)) | (month(min.date) == month(max.date) & day(max.date) > day(min.date)))
        max.date = as.Date(paste(year(max.date.reelle)+1,substr(min.date,6,10),sep="-"))
  
  # Calcul du pas de temps minimum entre 2 dates d'acquisition d'image satellite (surl'ensemble des polygones si plusieurs)
  ps.tps = unique(sort(donnees$date2))
  ps.tps2 = NULL
  for (j in 2:length(ps.tps)) ps.tps2 = c(ps.tps2,as.numeric(ps.tps[j] - ps.tps[j-1]))
  

  ### Decomposition des jeux de donnees en sous-jeux de donnees par polygone
  donnees = list(tablo=split(donnees,donnees$fid),
                 min.date=min.date,
                 max.date=max.date,
                 max.date.reelle=max.date.reelle,
                 pas.tps=ps.tps2,
                jeu.orig = jeu.orig)

  return(donnees)
}

NDVI = menf("NDVI")
NDWIGAO = menf("NDWIGAO")
MNDWI = menf("MNDWI")
rm(menf)


### Normalement, les indices ont le meme nombre de polygones et les polygones ont le meme identifiant pour chaque indice.
#### Le lissage se fait sur chaque polygone #####
sortie.id = mclapply(names(NDVI$tablo), function(i){
    ### Execution de la fonction qui repere les dates avec un probleme de detection de nuage
    detecloud = function(donnees,seuil.NA=75, periode=30, rmoy.NDVI=5, rsd.NDVI=7, rmoy.NDWIMCF=5, rsd.NDWIMCF=6, rmoy.NDWIGAO=4, rsd.NDWIGAO=8, rmoy.MNDWI=4, rsd.MNDWI=7){
      # On recupere des donnees du polygone
      jeu0 = donnees$tablo[[i]]
      jeu0 = jeu0[order(jeu0$date2),]
      # Creation d'une variable logique : probleme de detection de nuage
      jeu0$pb.detec = 0
      
      # Suppression des dates ayant plus de 75% de valeurs manquantes
      jeu = jeu0[jeu0$pct.na <= seuil.NA,]
      
      # Definition du l'indice etudie
      indice = unique(sapply(strsplit(levels(jeu0$filename),"_"),function(x) x[8]))
      
      ## On repere les dates qui seraient susceptibles d'avoir un probleme de detection a partir des boxplot de la moyenne et de l'ecart du polygone
      bxp.moy = boxplot.stats(jeu$mean)
      bxp.sd = boxplot.stats(jeu$std)
      # Pour l'indice NDVI, les problemes de detection de nuage donnent un indice tres faible (->0) avec tres peu de variabilite/dispersion
      # On selectionne les dates dont la moyenne de l'indice < a Q25 et dont l'ecart-type < a Q25.
      if (indice == "NDVI") jeu.ext = jeu[jeu$mean<bxp.moy$stats[2] & jeu$std<bxp.sd$stats[2],]
      # Pour l'indice NDVI, les problemes de detection de nuage donnent un indice tres eleve (->1) avec tres peu de variabilite/dispersion
      # On selectionne les dates dont la moyenne de l'indice > a Q75 et dont l'ecart-type < a Q25.
      if (indice %in% c("NDWIMCF","NDWIGAO","MNDWI")) jeu.ext = jeu[jeu$mean>bxp.moy$stats[4] & jeu$std<bxp.sd$stats[2],]
      
      # On etudie la variabilite a ces dates sur la moyenne et l'ecart-type sur une periode de 2 mois (1 mois avant et apres la date etudiee)
      cat(paste(i,length(unique(jeu.ext$date2))), "\n")
      
      for (j in unique(jeu.ext$date2)){
        # Etude sur une courte periode de 2 mois (1 mois avant et 1 mois apres la date etudiee)
        ss.jeu = jeu[jeu$date2>=(j - periode) & jeu$date2<=(j + periode),]
        
        if (indice == "NDVI"){
          ## Si on n'a qu'une valeur dans le sous-jeu de donnee ou qu'il y a plusieurs valeurs, mais qu'elles sont incluses dans la liste selectionnee dans jeu.ext, alors on compare la valeur de cette date sur l'ensemble du jeu de donnees du polygone (moins la date) :
          if (nrow(ss.jeu) == 1 | (nlevels(factor(ss.jeu$date2[ss.jeu$date2 %in% jeu.ext$date2]))==nrow(ss.jeu))){
            # La moyenne de l'ensemble des donnees (sans la date) est toujours > a la moyenne a la date pour NDVI. Si la moyenne a la date et la moyenne de l'ensemble du jeu de donnees (sans la date) sont de meme signe (+/-), alors on calcule le ratio moyenne de l'ensemble / moyenne de la date, sinon la moyenne a la date est negative, alors on calcule le ratio moyenne de l'ensemble / 0.0001.
            # -> Si le ratio > 5 et
            # -> Si l'ecart-type a la date < a l'ecart-type moyen (sans la date) avec un ratio > 10,
            # => Alors on decide qu'il y a un probleme de detection de nuage.
            if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 2){
              if (mean(jeu$mean[jeu$date2!=j])/jeu$mean[jeu$date2==j] > rmoy.NDVI & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDVI) jeu0$pb.detec[jeu0$date2==j] = 1} else 
                if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 0){
                  if (1/(mean(jeu$mean[jeu$date2!=j])/jeu$mean[jeu$date2==j]) > rmoy.NDVI & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDVI) jeu0$pb.detec[jeu0$date2==j] = 1} else 
                    if (mean(jeu$mean[jeu$date2!=j])/0.0001 > rmoy.NDVI & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDVI) jeu0$pb.detec[jeu0$date2==j] = 1} else {
                      ## Cas ou on a plusieurs valeurs dont des dates qui ne sont pas incluses dans la liste jeu.ext, on compare les donnees de la date etudiee a l'ensemble du sous-jeu de donnees
                      # Statistiques descriptives a partir de la moyenne
                      stat.moy = cbind(fid=i,date2=j,val.obs=ss.jeu$mean[ss.jeu$date2==j],psych::describe(ss.jeu$mean))
                      # Statistiques descriptives a partir de l'ecart-type (sans l'ecart-type de la date)
                      stat.sd = cbind(data.frame(fid=i,date2=j,val.obs=ss.jeu$std[ss.jeu$date2==j],mean=mean(ss.jeu$std[ss.jeu$date2!=j])))
                      # Si la moyenne est la valeur minimum du sous-jeu et si le ratio moyenne des ecarts-type (sans la date)/ecart-type de la date > 10, alors on decide qu'il y a un probleme de detection de nuage (NB : fonnctionne aussi dans le cas ou on a plus de date incluses dans jeu.text sur l'ensemble des dates)
                      if (stat.moy$val.obs != stat.moy$min & stat.sd$mean/stat.sd$val.obs > rsd.NDVI){
                        if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 2){
                          if (mean(jeu$mean[jeu$date2!=j])/jeu$mean[jeu$date2==j] > rmoy.NDVI) jeu0$pb.detec[jeu0$date2==j] = 1} else 
                            if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 0){
                              if (1/(mean(jeu$mean[jeu$date2!=j])/jeu$mean[jeu$date2==j]) > rmoy.NDVI) jeu0$pb.detec[jeu0$date2==j] = 1} else
                                if (mean(jeu$mean[jeu$date2!=j])/0.0001 > rmoy.NDVI) jeu0$pb.detec[jeu0$date2==j] = 1}
                      if (stat.moy$val.obs == stat.moy$min & stat.sd$mean/stat.sd$val.obs > rsd.NDVI) jeu0$pb.detec[jeu0$date2==j] = 1
                    }}
        
        if (indice == "NDWIMCF"){
          ## Si on n'a qu'une valeur dans le sous-jeu de donnee ou qu'il y a plusieurs valeurs, mais qu'elles sont incluses dans la liste selectionnee dans jeu.ext, alors on compare la valeur de cette date sur l'ensemble du jeu de donnees du polygone (moins la date) :
          if (nrow(ss.jeu) == 1 | (nlevels(factor(ss.jeu$date2[ss.jeu$date2 %in% jeu.ext$date2]))==nrow(ss.jeu))){
            # Pour NDWI de Gao, la moyenne de l'ensemble des donnees (sans la date) est toujours < a la moyenne a la date. Si la moyenne a la date et la moyenne de l'ensemble du jeu de donnzes (sans la date) sont de mzme signe (+/-), alors on calcule le ratio moyenne de la date / moyenne de l'ensemble, sinon la moyenne de l'ensemble est negative, alors on calcule le ratio moyenne de la date / 0.0001.
            # -> Si le ratio > 5 et
            # -> Si l'ecart-type e la date < a l'ecart-type moyen (sans la date) avec un ratio > 10,
            # => Alors on decide qu'il y a un probleme de detection de nuage.
            if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 2){
              if (jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j]) > rmoy.NDWIMCF & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDWIMCF) jeu0$pb.detec[jeu0$date2==j] = 1} else 
                if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 0){
                  if (1/(jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j])) > rmoy.NDWIMCF & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDWIMCF) jeu0$pb.detec[jeu0$date2==j] = 1} else 
                    if (jeu$mean[jeu$date2==j]/0.0001 > rmoy.NDWIMCF & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDWIMCF) jeu0$pb.detec[jeu0$date2==j] = 1} else {
                      ## Cas ou on a plusieurs valeurs dont des dates qui ne sont pas incluses dans la liste jeu.ext, on compare les donnees de la date etudiee a l'ensemble du sous-jeu de donnees
                      # Statistiques descriptives a partir de la moyenne
                      stat.moy = cbind(fid=i,date2=j,val.obs=ss.jeu$mean[ss.jeu$date2==j],psych::describe(ss.jeu$mean))
                      # Statistiques descriptives a partir de l'ecart-type (sans l'ecart-type de la date)
                      stat.sd = cbind(data.frame(fid=i,date2=j,val.obs=ss.jeu$std[ss.jeu$date2==j],mean=mean(ss.jeu$std[ss.jeu$date2!=j])))
                      # Si la moyenne est la valeur maximum du sous-jeu et si le ratio moyenne des ecarts-type (sans la date)/ecart-type de la date > 10, alors on decide qu'il y a un probleme de detection de nuage (NB : fonnctionne aussi dans le cas ou on a plus de dates incluses dans jeu.text sur l'ensemble des dates)
                      if (stat.moy$val.obs != stat.moy$max & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDWIMCF){
                        if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 2){
                          if (jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j]) > rmoy.NDWIMCF) jeu0$pb.detec[jeu0$date2==j] = 1} else
                            if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 0){
                              if (1/(jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j])) > rmoy.NDWIMCF) jeu0$pb.detec[jeu0$date2==j] = 1} else
                                if (jeu$mean[jeu$date2==j]/0.0001 > rmoy.NDWIMCF) jeu0$pb.detec[jeu0$date2==j] = 1}
                      if (stat.moy$val.obs == stat.moy$max & stat.sd$mean/stat.sd$val.obs > rsd.NDWIMCF) jeu0$pb.detec[jeu0$date2==j] = 1
                    }}
        
        if (indice == "NDWIGAO"){
          ## Si on n'a qu'une valeur dans le sous-jeu de donnee ou qu'il y a plusieurs valeurs, mais qu'elles sont incluses dans la liste selectionnee dans jeu.ext, alors on compare la valeur de cette date sur l'ensemble du jeu de donnees du polygone (moins la date) :
          if (nrow(ss.jeu) == 1 | (nlevels(factor(ss.jeu$date2[ss.jeu$date2 %in% jeu.ext$date2]))==nrow(ss.jeu))){
            # Pour NDWI de Gao, la moyenne de l'ensemble des donnees (sans la date) est toujours < a la moyenne a la date. Si la moyenne a la date et la moyenne de l'ensemble du jeu de donnees (sans la date) sont de meme signe (+/-), alors on calcule le ratio moyenne de la date / moyenne de l'ensemble, sinon la moyenne de l'ensemble est negative, alors on calcule le ratio moyenne de la date / 0.0001.
            # -> Si le ratio > 5 et
            # -> Si l'ecart-type a la date < a l'ecart-type moyen (sans la date) avec un ratio > 10,
            # => Alors on decide qu'il y a un probleme de detection de nuage.
            if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 2){
              if (jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j]) > rmoy.NDWIGAO & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDWIGAO) jeu0$pb.detec[jeu0$date2==j] = 1} else 
                if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 0){
                  if (1/(jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j])) > rmoy.NDWIGAO & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDWIGAO) jeu0$pb.detec[jeu0$date2==j] = 1} else 
                    if (jeu$mean[jeu$date2==j]/0.0001 > rmoy.NDWIGAO & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDWIGAO) jeu0$pb.detec[jeu0$date2==j] = 1} else {
                      ## Cas ou on a plusieurs valeurs dont des dates qui ne sont pas incluses dans la liste jeu.ext, on compare les donnees de la date etudiee a l'ensemble du sous-jeu de donnees
                      # Statistiques descriptives a partir de la moyenne
                      stat.moy = cbind(fid=i,date2=j,val.obs=ss.jeu$mean[ss.jeu$date2==j],psych::describe(ss.jeu$mean))
                      # Statistiques descriptives a partir de l'ecart-type (sans l'ecart-type de la date)
                      stat.sd = cbind(data.frame(fid=i,date2=j,val.obs=ss.jeu$std[ss.jeu$date2==j],mean=mean(ss.jeu$std[ss.jeu$date2!=j])))
                      # Si la moyenne est la valeur maximum du sous-jeu et si le ratio moyenne des ecarts-type (sans la date)/ecart-type de la date > 10, alors on decide qu'il y a un probleme de detection de nuage (NB : fonnctionne aussi dans le cas ou on a plus de date incluses dans jeu.text sur l'ensemble des dates)
                      if (stat.moy$val.obs != stat.moy$max & stat.sd$mean/stat.sd$val.obs > rsd.NDWIGAO){
                        if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 2){
                          if (jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j]) > rmoy.NDWIGAO) jeu0$pb.detec[jeu0$date2==j] = 1} else
                            if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 0){
                              if (1/(jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j])) > rmoy.NDWIGAO) jeu0$pb.detec[jeu0$date2==j] = 1} else
                                if (jeu$mean[jeu$date2==j]/0.0001 > rmoy.NDWIGAO) jeu0$pb.detec[jeu0$date2==j] = 1}
                      if (stat.moy$val.obs == stat.moy$max & stat.sd$mean/stat.sd$val.obs > rsd.NDWIGAO) jeu0$pb.detec[jeu0$date2==j] = 1
                    }}
        
        if (indice == "MNDWI"){
          ## Si on n'a qu'une valeur dans le sous-jeu de donnee ou qu'il y a plusieurs valeurs, mais qu'elles sont incluses dans la liste selectionnee dans jeu.ext, alors on compare la valeur de cette date sur l'ensemble du jeu de donnees du polygone (moins la date) :
          if (nrow(ss.jeu) == 1 | (nlevels(factor(ss.jeu$date2[ss.jeu$date2 %in% jeu.ext$date2]))==nrow(ss.jeu))){
            # Pour NDWI de Gao, la moyenne de l'ensemble des donnees (sans la date) est toujours < a la moyenne a la date. Si la moyenne a la date et la moyenne de l'ensemble du jeu de donnees (sans la date) sont de meme signe (+/-), alors on calcule le ratio moyenne de la date / moyenne de l'ensemble, sinon la moyenne de l'ensemble est negative, alors on calcule le ratio moyenne de la date / 0.0001.
            # -> Si le ratio > 5 et
            # -> Si l'ecart-type a la date < a l'ecart-type moyen (sans la date) avec un ratio > 10,
            # => Alors on decide qu'il y a un probleme de detection de nuage.
            if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 2){
              if (jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j]) > rmoy.MNDWI & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.MNDWI) jeu0$pb.detec[jeu0$date2==j] = 1} else 
                if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 0){
                  if (1/(jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j])) > rmoy.MNDWI & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.MNDWI) jeu0$pb.detec[jeu0$date2==j] = 1} else 
                    if (jeu$mean[jeu$date2==j]/0.0001 > rmoy.MNDWI & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.MNDWI) jeu0$pb.detec[jeu0$date2==j] = 1} else {
                      ## Cas ou on a plusieurs valeurs dont des dates qui ne sont pas incluses dans la liste jeu.ext, on compare les donnees de la date etudiee a l'ensemble du sous-jeu de donnees
                      # Statistiques descriptives a partir de la moyenne
                      stat.moy = cbind(fid=i,date2=j,val.obs=ss.jeu$mean[ss.jeu$date2==j],psych::describe(ss.jeu$mean))
                      # Statistiques descriptives a partir de l'ecart-type (sans l'ecart-type de la date)
                      stat.sd = cbind(data.frame(fid=i,date2=j,val.obs=ss.jeu$std[ss.jeu$date2==j],mean=mean(ss.jeu$std[ss.jeu$date2!=j])))
                      # Si la moyenne est la valeur maximum du sous-jeu et si le ratio moyenne des ecarts-type (sans la date)/ecart-type de la date > 10, alors on decide qu'il y a un probleme de detection de nuage (NB : fonnctionne aussi dans le cas ou on a plus de date incluses dans jeu.text sur l'ensemble des dates)
                      if (stat.moy$val.obs != stat.moy$max & stat.sd$mean/stat.sd$val.obs > rsd.MNDWI){
                        if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 2){
                          if (jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j]) > rmoy.MNDWI) jeu0$pb.detec[jeu0$date2==j] = 1} else
                            if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 0){
                              if (1/(jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j])) > rmoy.MNDWI) jeu0$pb.detec[jeu0$date2==j] = 1} else
                                if (jeu$mean[jeu$date2==j]/0.0001 > rmoy.MNDWI) jeu0$pb.detec[jeu0$date2==j] = 1}
                      if (stat.moy$val.obs == stat.moy$max & stat.sd$mean/stat.sd$val.obs > rsd.MNDWI) jeu0$pb.detec[jeu0$date2==j] = 1
                    }}
      }
      return(jeu0)
    }
    NDVI.id = detecloud(NDVI)
    NDWIGAO.id = detecloud(NDWIGAO)
    MNDWI.id = detecloud(MNDWI)
    rm(detecloud)
    
    # Conservation des variables utiles
    NDVI.cons = NDVI.id[,match(c("mean","std","date2","pct.na"),names(NDVI.id))]
    NDWIGAO.cons = NDWIGAO.id[,match(c("mean","std","date2","pct.na"),names(NDWIGAO.id))]
    MNDWI.cons = MNDWI.id[,match(c("mean","std","date2","pct.na"),names(MNDWI.id))]
    
    # Renommage des noms de variables
    names(NDVI.cons)[match(c("mean","std"),names(NDVI.cons))] = paste("NDVI",c("mean","std"),sep=".")
    names(NDWIGAO.cons)[match(c("mean","std"),names(NDWIGAO.cons))] = paste("NDWIGAO",c("mean","std"),sep=".")
    names(MNDWI.cons)[match(c("mean","std"),names(MNDWI.cons))] = paste("MNDWI",c("mean","std"),sep=".")
    
    # Concatenation des 4 jeux de donn�es
    jeu1 = merge(NDVI.cons,NDWIGAO.cons,by=c("date2","pct.na"))
    jeu.ab = merge(jeu1,MNDWI.cons,by=c("date2","pct.na"))
    rm(NDVI.cons,NDWIGAO.cons,MNDWI.cons,jeu1)
    
    # Definition des seuils sur l'ensemble des indices
    jeu.ab$date.ab = 0
    jeu.ab$date.ab[jeu.ab$pct.na<=seuil.NA & 
        round(jeu.ab$NDVI.mean,digits=1)<=0.1 & round(jeu.ab$NDVI.std,digits=1)<=0.2 & 
        round(jeu.ab$NDWIGAO.mean,digits=1)>=0.5 & round(jeu.ab$NDWIGAO.std,digits=1)<=0.3 & 
        round(jeu.ab$MNDWI.mean,digits=1)>=0.5 & round(jeu.ab$MNDWI.std,digits=1)<=0.3] = 1
    jeu.ab = jeu.ab[,c("date2","pct.na","date.ab")]

    
    
    ##### Execution du lissage par transformation de Fourier pour chaque polygone #####
    lissage = function(donnees,jeu){

      # Definition du l'indice etudie
      indice = unique(sapply(strsplit(levels(jeu$filename),"_"),function(x) x[8]))
      
      # Ajout de la nouvelle variable sur chaque jeu.ab de donnees d'indice
      jeu = merge(jeu,jeu.ab,by=c("date2","pct.na"),all.x=TRUE)
      jeu$pb.detec[jeu$date.ab==1] = 1
      jeu = jeu[,-match("date.ab",names(jeu))]

      # Selection des valeurs brutes qui seront conservees pour l'analyse
      jeu$raw.data = jeu$mean
      jeu$raw.data[jeu$pct.na > seuil.NA | jeu$pb.detec==1] = NA  # Suppression des valeurs avec une couverture nuageuse >75% et avec un probl�me de d�tection de nuage
      

      # Realiser 5 sous-jeux de donnees
      set.seed(190382)
      echant = createFolds(1:nrow(jeu),k=5,returnTrain=TRUE)
      
      
      ### Calcul des seuils optimaux pour le modele de Transformation de Fourier pour chaque jeu d'apprentissage et verification avec le jeu-test
      seuil.mod23 = t(data.frame(lapply(echant,function(x){
        # Creation des jeux d'apprentissage et des jeux-test
        jeu.train = jeu[x,]
        jeu.test = jeu[-x,]
        
        ## Imputation du jeu de donnees si le pas de temps minimum vaut 5 jours
        if (min(donnees$pas.tps) == 5){
          # 1ere imputation : uniquement sur la periode ou on a des observations sur le polygone
          jours0 = data.frame(date2=seq(min(jeu.train$date2),max(jeu.train$date2),by=5))
          jeu0 = merge(jours0,jeu.train,by="date2",all.x=TRUE)
          jeu0 = jeu0[order(jeu0$date2),]
          jeu0$don.imp = jeu0$raw.data
          # Utilisation de l'imputation par moyenne-mobile (sauf pour MNDWI : moyenne)
          test = ts(jeu0$don.imp,frequency=73,start=c(year(min(jeu0$date2)),yday(min(jeu0$date2))/5))
          jeu0$don.imp = as.numeric(na_seadec(test,algorithm="ma",find_frequency=TRUE))
          # Surtout ne pas utiliser "mean" pour MNDWI car cela cree des saisonnalites quand il n'y en a pas !
          
          # 2eme imputation : sur toute la periode etudiee
          jours1 = data.frame(date2=seq(donnees$min.date,donnees$max.date,by=5))
          jeu.train = merge(jours1,jeu0,by="date2",all.x=TRUE)
          jeu.train$fid[is.na(jeu.train$fid)] = i
          test = ts(jeu.train$don.imp,frequency=73,start=c(year(min(jeu.train$date2)),yday(min(jeu.train$date2))/5))
          # Utilisation de l'imputation par la moyenne sur la saisonnalite
          jeu.train$don.imp = as.numeric(na_seasplit(test,algorithm="mean",find_frequency=TRUE))
        } else {
          ## Imputation du jeu de donnees si le pas de temps minimum n'est pas 5
          # 1ere imputation : uniquement sur la periode ou on a des observations sur le polygone
          jours0 = data.frame(date2=seq(min(jeu.train$date2),max(jeu.train$date2),by=1))
          jeu0 = merge(jours0,jeu.train,by="date2",all.x=TRUE)
          jeu0 = jeu0[order(jeu0$date2),]
          jeu0$don.imp = jeu0$raw.data
          # Utilisation de l'imputation par moyenne-mobile
          if (yday(min(jeu0$date2)) <= 365) test = ts(jeu0$don.imp,frequency=365,start=c(year(min(jeu0$date2)),yday(min(jeu0$date2)))) else
            test = ts(jeu0$don.imp,frequency=365,start=c(year(min(jeu0$date2)),365))
          jeu0$don.imp = as.numeric(na_seadec(test,algorithm="ma",find_frequency=TRUE))
          # Surtout ne pas utiliser "mean" pour MNDWI car cela cree des saisonnalites quand il n'y en a pas !
          
          # 2eme imputation : sur toute la periode etudiee
          jours1 = data.frame(date2=seq(donnees$min.date,donnees$max.date,by=1))
          jeu.train = merge(jours1,jeu0,by="date2",all.x=TRUE)
          jeu.train$fid[is.na(jeu.train$fid)] = i
          if (yday(min(jeu.train$date2)) <= 365) test = ts(jeu.train$don.imp,frequency=365,start=c(year(min(jeu.train$date2)),yday(min(jeu.train$date2)))) else 
            test = ts(jeu.train$don.imp,frequency=365,start=c(year(min(jeu.train$date2)),365))
          # Utilisation de l'imputation par la moyenne sur la saisonnalite
          jeu.train$don.imp = as.numeric(na_seasplit(test,algorithm="mean",find_frequency=TRUE))
        }
        
        ### Lissage par transformation de Fourier (avec imputation des donnees)
        # Etude sur un seuil d'ondelette compris entre 1 et 12% du jeu de donnees
        nb.seuil = unique(round(seq(1,12,by=1)*nrow(jeu.train)/100,digits=0))
        
        ## 2eme modele : lissage par transformation de Fourier sur plusieurs seuils
        lissage.fourier = lapply(nb.seuil,function(seuil){
          if(odd(seuil) == FALSE) seuil = seuil + 1
          fct.fourier = create.fourier.basis(rangeval=range(as.numeric(jeu.train$date2)),nbasis=seuil)
          modele2 = smooth.basis(argvals=as.numeric(jeu.train$date2),y=jeu.train$don.imp,fdParobj=fct.fourier)$fd
          lissage2 = eval.fd(as.numeric(jeu.train$date2),modele2)
          data.frame(seuil=seuil,date2=jeu.train$date2,lissage2=lissage2)
        })
        names(lissage.fourier) = nb.seuil
        
        erreur.F = unlist(lapply(lissage.fourier,function(res.seuil){
          observe = jeu.test[!is.na(jeu.test$raw.data),c("date2","raw.data")]
          predit = res.seuil[res.seuil$date2 %in% jeu.test$date2[!is.na(jeu.test$raw.data)],c("date2","lissage2")]
          concat = merge(observe,predit,by="date2")
          rmse(concat$raw.data,concat$lissage2)
        }))
        # Definition du seuil optimal pour Fourier
        seuil.F = as.numeric(names(erreur.F)[erreur.F==min(erreur.F)])
        if (length(seuil.F) > 1) seuil.F = round(mean(seuil.F),digits=0)
        
        seuil.opt = seuil.F*100/nrow(jeu.train)
        names(seuil.opt) = "Fourier"
        seuil.opt 
      })))
      
      
      ## Imputation du jeu de donnees si le pas de temps minimum vaut 5 jours
      if (min(donnees$pas.tps) == 5){
        # 1ere imputation : uniquement sur la periode oe on a des observations sur le polygone
        jours0 = data.frame(date2=seq(min(jeu$date2),max(jeu$date2),by=5))
        jeu0 = merge(jours0,jeu,by="date2",all.x=TRUE)
        jeu0 = jeu0[order(jeu0$date2),]
        jeu0$don.imp = jeu0$raw.data
        # Utilisation de l'imputation par moyenne-mobile (sauf pour MNDWI : moyenne)
        test = ts(jeu0$don.imp,frequency=73,start=c(year(min(jeu0$date2)),yday(min(jeu0$date2))/5))
        jeu0$don.imp = as.numeric(na_seadec(test,algorithm="ma",find_frequency=TRUE))
        # Surtout ne pas utiliser "mean" pour MNDWI car cela cree des saisonnalites quand il n'y en a pas !
        
        # 2eme imputation : sur toute la periode etudiee
        jours1 = data.frame(date2=seq(donnees$min.date,donnees$max.date,by=5))
        jeu = merge(jours1,jeu0,by="date2",all.x=TRUE)
        jeu$fid[is.na(jeu$fid)] = i
        test = ts(jeu$don.imp,frequency=73,start=c(year(min(jeu$date2)),yday(min(jeu$date2))/5))
        # Utilisation de l'imputation par la moyenne sur la saisonnalite
        jeu$don.imp = as.numeric(na_seasplit(test,algorithm="mean",find_frequency=TRUE))
      } else {
        ## Imputation du jeu de donnees si le pas de temps minimum n'est pas 5
        # 1ere imputation : uniquement sur la periode ou on a des observations sur le polygone
        jours0 = data.frame(date2=seq(min(jeu$date2),max(jeu$date2),by=1))
        jeu0 = merge(jours0,jeu,by="date2",all.x=TRUE)
        jeu0 = jeu0[order(jeu0$date2),]
        jeu0$don.imp = jeu0$raw.data
        # Utilisation de l'imputation par moyenne-mobile (sauf pour MNDWI : moyenne)
        if (yday(min(jeu0$date2)) <= 365) test = ts(jeu0$don.imp,frequency=365,start=c(year(min(jeu0$date2)),yday(min(jeu0$date2)))) else
          test = ts(jeu0$don.imp,frequency=365,start=c(year(min(jeu0$date2)),365))
        jeu0$don.imp = as.numeric(na_seadec(test,algorithm="ma",find_frequency=TRUE))
        # Surtout ne pas utiliser "mean" pour MNDWI car cela cree des saisonnalites quand il n'y en a pas !
        
        # 2eme imputation : sur toute la periode etudiee
        jours1 = data.frame(date2=seq(donnees$min.date,donnees$max.date,by=1))
        jeu = merge(jours1,jeu0,by="date2",all.x=TRUE)
        jeu$fid[is.na(jeu$fid)] = i
        if (yday(min(jeu$date2)) <= 365) test = ts(jeu$don.imp,frequency=365,start=c(year(min(jeu$date2)),yday(min(jeu$date2)))) else 
          test = ts(jeu$don.imp,frequency=365,start=c(year(min(jeu$date2)),365))
        # Utilisation de l'imputation par la moyenne sur la saisonnalite
        jeu$don.imp = as.numeric(na_seasplit(test,algorithm="mean",find_frequency=TRUE))
      }
      
      
      # Calcul de la moyenne de chaque seuil par groupe de jeux
      seuil.mod23 = apply(seuil.mod23,2,function(x) round(mean(x)*nrow(jeu)/100,digits=0))
      
      ## 2eme modele : lissage par transformation de Fourier
      # Execution du modele au seuil optimal
      if(odd(seuil.mod23["Fourier"]) == FALSE) seuil.mod23["Fourier"] = seuil.mod23["Fourier"] + 1
      fct.fourier = create.fourier.basis(rangeval=range(as.numeric(jeu$date2)),nbasis=seuil.mod23["Fourier"])
      modele2 = smooth.basis(argvals=as.numeric(jeu$date2),y=jeu$don.imp,fdParobj=fct.fourier)$fd
      seuil.F = seuil.mod23["Fourier"]*100/nrow(jeu)
      
      
      ### Lissage sur toutes les dates de la periode totale pour les 3 modeles
      jours = seq(donnees$min.date,donnees$max.date.reelle,by=5)
      lissage2 = as.numeric(eval.fd(as.numeric(jours),modele2))
      
      ### Insertion dans le jeu de donnees final
      resultat = data.frame(index=indice,fid=i,date2=jours,fourier=lissage2,seuil.F=as.numeric(seuil.F),stringsAsFactors=TRUE)
      # On r�cup�re les autres variables du jeu de donn�es
      jeu = merge(resultat,jeu,by=c("fid","date2"),all.x=TRUE)
      # Rajout des informations d'origine a partir du fid
      jeu = merge(jeu,donnees$jeu.orig,by="fid",all.x=TRUE)
      jeu
    }
    list(NDVI = lissage(NDVI,NDVI.id),
         NDWIGAO = lissage(NDWIGAO,NDWIGAO.id),
         MNDWI = lissage(MNDWI,MNDWI.id))
  }
  ,mc.cores=nb.coeurs)
names(sortie.id) = names(NDVI$tablo)




#######################################################################
##### Mise en forme d'un jeu de donnees par indice, d'une table avec le modele optimal / polygone et exportation des fichiers par indice #####
### Concatener les resultats pour chaque indice et les exporter
mclapply(c("NDVI","NDWIGAO","MNDWI"), function(indice){
  # Lissage
  sortie = NULL
  for (i in names(sortie.id)) sortie = rbind(sortie,sortie.id[[i]][[indice]])
  # out_names
  outname_rds = paste("Yangon_hexasurv_named_",indice,"_smooth_current.rds",sep="")
  # Exportation des fichiers en format .rds
  saveRDS(sortie, outname_rds)
},mc.cores=nb.coeurs)



##### Representation graphique des lissages #####
graphique = function(indice){
  donnees = readRDS(paste("Yangon_hexasurv_named_",indice,"_smooth_current.rds",sep=""))
  # Representation graphique des 3 types de lissage
  pdf(paste("Yangon_hexasurv_named_",indice,"_smooth_current_graph.pdf",sep=""),width=12,height=8)
  layout(matrix(1:9,nrow=3,ncol=3,byrow=TRUE))
  for (i in levels(donnees$fid)){
    plot(mean~date2,data=donnees,subset=(fid==i),main=paste(indice,paste("fid",i,sep=" "),sep=" - "),las=1)
    points(mean~date2,data=donnees,subset=(fid==i & pct.na>75),col=2,pch=16)
    points(mean~date2,data=donnees,subset=(fid==i & pb.detec==1),col=3,pch=16)
    lines(fourier~date2,data=donnees,subset=(fid==i),col=4)
  }
  plot(0,type="n",xaxt="n",yaxt="n",bty="n",xlab="",ylab="")
  legend("center",legend=c(paste("Raw data",c("included in the model",paste("with > ",seuil.NA,"% NA",sep=""),paste("<= ",seuil.NA,"% NA with detection cloud's problem",sep="")),sep=" "),"Fitted data : Fourier transformation"),title="Legend",col=c(1:3,4),lty=c(rep(NA,3),1),pch=c(1,rep(16,2),NA))
  dev.off()
}

graphique("NDVI")
graphique("NDWIGAO")
graphique("MNDWI")

