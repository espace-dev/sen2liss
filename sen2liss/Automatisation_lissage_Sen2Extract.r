#!/usr/bin/Rscript

################################################################################
########## ESTIMATION D'UN LISSAGE SUR LES DONNEES BRUTES ##########
################################################################################



##### Dans le cas où on a plusieurs polygones/points, la période estimée représentera la date min et la date max où on a des observations sur au moins un polygone #####
##### Dans le cas où on a un polygone/point, la période estimée représentera uniquement celle où on a des observations dessus #####


# Modules
library(lubridate)
library(fda)
library(fda.usc)
library(Metrics)
library(imputeTS)
library(Hmisc)
library(gtools)
library(McSpatial)
library(caret)
library(parallel)

# Répertoire de travail
#setwd("D:/Sylvaine/Stage Pasteur")
#~ setwd("H:/Ird/Sylvaine/Stage Pasteur/3 ans d'études")
setwd(commandArgs(trailingOnly = TRUE)[1])


# Modification des couleurs par défaut
palette(c("black","red","green3","blue","cyan","magenta","yellow","gray"))


##### Dans le cas où on a plusieurs indices qui sont extraits, on réalise une boucle pour chaque indice #####
for (nom.fichier in list.files(".", pattern = "*.csv")){
  
  ### Importation du jeu de données et mise en forme des données ###
  donnees = read.csv(nom.fichier,stringsAsFactors=TRUE)

  # Définition du jeu d'origine avant extraction + fid
  jeu.orig = unique(donnees[,-match(c("date","tile","filename","count","nodata","nbcld","cldprb","min","max","mean","std","median","percentile_25","percentile_75"),names(donnees))])

  # Conservation des variables d'extraction uniquement
  donnees = donnees[,match(c("date","fid","tile","filename","count","nodata","nbcld","cldprb","min","max","mean","std","median","percentile_25","percentile_75"),names(donnees))]

  
  # Création d'une variable date en format date
  donnees$date2 = as.Date(donnees$date, "%Y-%m-%d")
  
  # Changement de format pour fid qui correspond aux identifiants des polygones
  donnees$fid = factor(donnees$fid,levels=min(donnees$fid):max(donnees$fid))
  
  # Création d'une variable part de NAs dans le buffer
  donnees$pct.na = round(donnees$nodata / (donnees$count + donnees$nodata) * 100,digits=2)
  
  # Modification des indices environnementaux pour être pris entre -1 et 1
  for (i in c("min","max","mean","std","median","percentile_25","percentile_75")) donnees[,i] = round(donnees[,i]/10000,digits=4)
  rm(i)
  
  # Pourcentage de nodata (à étudier)
  seuil.NA = 75
  
  # Définition du l'indice étudié
  indice = unique(sapply(strsplit(levels(donnees$filename),"_"),function(x) x[8]))
  
  # Calcul de la durée de la période observée
  diff.date = as.numeric(max(donnees$date2)-min(donnees$date2))
  
  
  ##### Au préalable, analyse avec un lissage par spline cubique, par transformation de Fourier et par B-spline pour chaque polygone #####
  ### Imputation sur toutes les dates d'acquisition d'images satellites
  # Calcul pour avoir une estimation sur des années complètes (sur la même période pour tous les polygones)
  min.date = min(donnees$date2)   # A partir de la 1ère date où on a des observations sur au moins un polygone
  ## Modification de la dernière date d'observation pour avoir des cycles complets (années complètes)
  max.date = max(donnees$date2)
  if ((month(max.date) < month(min.date)) | (month(min.date) == month(max.date) & day(max.date) < day(min.date)))
    max.date = as.Date(paste(max(year(donnees$date2)),substr(min.date,6,10),sep="-")) else
      if ((month(max.date) > month(min.date)) | (month(min.date) == month(max.date) & day(max.date) > day(min.date)))
        max.date = as.Date(paste(max(year(donnees$date2))+1,substr(min.date,6,10),sep="-"))
  
  # Décomposition du jeu de données en un sous-jeu de données par polygone
  tablo = split(donnees,donnees$fid)
  
  # Calcul du pas de temps minimum entre 2 dates d'acquisition d'image satellite par polygone
  pas.tps = sapply(tablo,function(x){
    ps.tps = unique(sort(x$date2))
    ps.tps2 = NULL
    for (j in 2:length(ps.tps)) ps.tps2 = c(ps.tps2,as.numeric(ps.tps[j] - ps.tps[j-1]))
    min(ps.tps2)
  })
  
  # Calcul du nombre de coeurs sur le serveur
  nb.coeurs = detectCores()
  
  
  
  
  
  ################################################################################
  ########## Etude sur les valeurs extrêmes par polygone ##########
  detecloud = function(jeu0,seuil.NA=75, periode=30, rmoy.NDVI=5, rsd.NDVI=7, rmoy.NDWIMCF=5, rsd.NDWIMCF=6, rmoy.NDWIGAO=4, rsd.NDWIGAO=8, rmoy.MNDWI=4, rsd.MNDWI=7){
    # On supprime les dates ayant plus de 75% de valeurs manquantes
    jeu0 = jeu0[order(jeu0$date2),]
    
    # Création d'une variable logique : problème de détection de nuage
    jeu0$pb.detec = 0
    
    # Suppression des dates ayant plus de 75% de couverture nuageuse
    jeu = jeu0[jeu0$pct.na <= seuil.NA,]
    
    ## On repère les dates qui seraient susceptibles d'avoir un problème de détection à partir des boxplot de la moyenne et de l'écart du polygone
    bxp.moy = boxplot.stats(jeu$mean)
    bxp.sd = boxplot.stats(jeu$std)
    # Pour l'indice NDVI, les problèmes de détection de nuage donnent un indice très faible (->0) avec très peu de variabilité/dispersion
    # On sélectionne les dates dont la moyenne de l'indice < à Q25 et dont l'écart-type < à Q25.
    if (indice == "NDVI") jeu.ext = jeu[jeu$mean<bxp.moy$stats[2] & jeu$std<bxp.sd$stats[2],]
    # Pour l'indice NDVI, les problèmes de détection de nuage donnent un indice très élevé (->1) avec très peu de variabilité/dispersion
    # On sélectionne les dates dont la moyenne de l'indice > à Q75 et dont l'écart-type < à Q25.
    if (indice %in% c("NDWIMCF","NDWIGAO","MNDWI")) jeu.ext = jeu[jeu$mean>bxp.moy$stats[4] & jeu$std<bxp.sd$stats[2],]
    
    # On étudie la variabilité à ces dates sur la moyenne et l'écart-type sur une période de 2 mois (1 mois avant et après la date étudiée)
    #cat(paste(unique(jeu0$fid),length(unique(jeu.ext$date2))), "\n")
    
    for (j in unique(jeu.ext$date2)){
      # Etude sur une courte période de 2 mois (1 mois avant et 1 mois après la date étudiée)
      ss.jeu = jeu[jeu$date2>=(j - periode) & jeu$date2<=(j + periode),]
      
      if (indice == "NDVI"){
        ## Si on n'a qu'une valeur dans le sous-jeu de donnée ou qu'il y a plusieurs valeurs, mais qu'elles sont incluses dans la liste sélectionnée dans jeu.ext, alors on compare la valeur de cette date sur l'ensemble du jeu de données du polygone (moins la date) :
        if (nrow(ss.jeu) == 1 | (nlevels(factor(ss.jeu$date2[ss.jeu$date2 %in% jeu.ext$date2]))==nrow(ss.jeu))){
          # La moyenne de l'ensemble des données (sans la date) est toujours > à la moyenne à la date pour NDVI. Si la moyenne à la date et la moyenne de l'ensemble du jeu de données (sans la date) sont de même signe (+/-), alors on calcule le ratio moyenne de l'ensemble / moyenne de la date, sinon la moyenne à la date est négative, alors on calcule le ratio moyenne de l'ensemble / 0.0001.
          # -> Si le ratio > 5 et
          # -> Si l'écart-type à la date < à l'écart-type moyen (sans la date) avec un ratio > 10,
          # => Alors on décide qu'il y a un problème de détection de nuage.
          if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 2){
            if (mean(jeu$mean[jeu$date2!=j])/jeu$mean[jeu$date2==j] > rmoy.NDVI & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDVI) jeu0$pb.detec[jeu0$date2==j] = 1} else 
              if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 0){
                if (1/(mean(jeu$mean[jeu$date2!=j])/jeu$mean[jeu$date2==j]) > rmoy.NDVI & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDVI) jeu0$pb.detec[jeu0$date2==j] = 1} else 
                  if (mean(jeu$mean[jeu$date2!=j])/0.0001 > rmoy.NDVI & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDVI) jeu0$pb.detec[jeu0$date2==j] = 1} else {
                    ## Cas où on a plusieurs valeurs dont des dates qui ne sont pas incluses dans la liste jeu.ext, on compare les données de la date étudiée à l'ensemble du sous-jeu de données
                    # Statistiques descriptives à partir de la moyenne
                    stat.moy = cbind(fid=unique(jeu0$fid),date2=j,val.obs=ss.jeu$mean[ss.jeu$date2==j],psych::describe(ss.jeu$mean))
                    # Statistiques descriptives à partir de l'écart-type (sans l'écart-type de la date)
                    stat.sd = cbind(data.frame(fid=unique(jeu0$fid),date2=j,val.obs=ss.jeu$std[ss.jeu$date2==j],mean=mean(ss.jeu$std[ss.jeu$date2!=j])))
                    # Si la moyenne est la valeur minimum du sous-jeu et si le ratio moyenne des écarts-type (sans la date)/écart-type de la date > 10, alors on décide qu'il y a un problème de détection de nuage (NB : fonnctionne aussi dans le cas où on a plus de date incluses dans jeu.text sur l'ensemble des dates)
                    if (stat.moy$val.obs != stat.moy$min & stat.sd$mean/stat.sd$val.obs > rsd.NDVI){
                      if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 2){
                        if (mean(jeu$mean[jeu$date2!=j])/jeu$mean[jeu$date2==j] > rmoy.NDVI) jeu0$pb.detec[jeu0$date2==j] = 1} else 
                          if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 0){
                            if (1/(mean(jeu$mean[jeu$date2!=j])/jeu$mean[jeu$date2==j]) > rmoy.NDVI) jeu0$pb.detec[jeu0$date2==j] = 1} else
                              if (mean(jeu$mean[jeu$date2!=j])/0.0001 > rmoy.NDVI) jeu0$pb.detec[jeu0$date2==j] = 1}
                    if (stat.moy$val.obs == stat.moy$min & stat.sd$mean/stat.sd$val.obs > rsd.NDVI) jeu0$pb.detec[jeu0$date2==j] = 1
                  }}
      
      if (indice == "NDWIMCF"){
        ## Si on n'a qu'une valeur dans le sous-jeu de donnée ou qu'il y a plusieurs valeurs, mais qu'elles sont incluses dans la liste sélectionnée dans jeu.ext, alors on compare la valeur de cette date sur l'ensemble du jeu de données du polygone (moins la date) :
        if (nrow(ss.jeu) == 1 | (nlevels(factor(ss.jeu$date2[ss.jeu$date2 %in% jeu.ext$date2]))==nrow(ss.jeu))){
          # Pour NDWI de Gao, la moyenne de l'ensemble des données (sans la date) est toujours < à la moyenne à la date. Si la moyenne à la date et la moyenne de l'ensemble du jeu de données (sans la date) sont de même signe (+/-), alors on calcule le ratio moyenne de la date / moyenne de l'ensemble, sinon la moyenne de l'ensemble est négative, alors on calcule le ratio moyenne de la date / 0.0001.
          # -> Si le ratio > 5 et
          # -> Si l'écart-type à la date < à l'écart-type moyen (sans la date) avec un ratio > 10,
          # => Alors on décide qu'il y a un problème de détection de nuage.
          if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 2){
            if (jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j]) > rmoy.NDWIMCF & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDWIMCF) jeu0$pb.detec[jeu0$date2==j] = 1} else 
              if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 0){
                if (1/(jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j])) > rmoy.NDWIMCF & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDWIMCF) jeu0$pb.detec[jeu0$date2==j] = 1} else 
                  if (jeu$mean[jeu$date2==j]/0.0001 > rmoy.NDWIMCF & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDWIMCF) jeu0$pb.detec[jeu0$date2==j] = 1} else {
                    ## Cas où on a plusieurs valeurs dont des dates qui ne sont pas incluses dans la liste jeu.ext, on compare les données de la date étudiée à l'ensemble du sous-jeu de données
                    # Statistiques descriptives à partir de la moyenne
                    stat.moy = cbind(fid=unique(jeu0$fid),date2=j,val.obs=ss.jeu$mean[ss.jeu$date2==j],psych::describe(ss.jeu$mean))
                    # Statistiques descriptives à partir de l'écart-type (sans l'écart-type de la date)
                    stat.sd = cbind(data.frame(fid=unique(jeu0$fid),date2=j,val.obs=ss.jeu$std[ss.jeu$date2==j],mean=mean(ss.jeu$std[ss.jeu$date2!=j])))
                    # Si la moyenne est la valeur maximum du sous-jeu et si le ratio moyenne des écarts-type (sans la date)/écart-type de la date > 10, alors on décide qu'il y a un problème de détection de nuage (NB : fonnctionne aussi dans le cas où on a plus de dates incluses dans jeu.text sur l'ensemble des dates)
                    if (stat.moy$val.obs != stat.moy$max & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDWIMCF){
                      if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 2){
                        if (jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j]) > rmoy.NDWIMCF) jeu0$pb.detec[jeu0$date2==j] = 1} else
                          if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 0){
                            if (1/(jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j])) > rmoy.NDWIMCF) jeu0$pb.detec[jeu0$date2==j] = 1} else
                              if (jeu$mean[jeu$date2==j]/0.0001 > rmoy.NDWIMCF) jeu0$pb.detec[jeu0$date2==j] = 1}
                    if (stat.moy$val.obs == stat.moy$max & stat.sd$mean/stat.sd$val.obs > rsd.NDWIMCF) jeu0$pb.detec[jeu0$date2==j] = 1
                  }}
      
      if (indice == "NDWIGAO"){
        ## Si on n'a qu'une valeur dans le sous-jeu de donnée ou qu'il y a plusieurs valeurs, mais qu'elles sont incluses dans la liste sélectionnée dans jeu.ext, alors on compare la valeur de cette date sur l'ensemble du jeu de données du polygone (moins la date) :
        if (nrow(ss.jeu) == 1 | (nlevels(factor(ss.jeu$date2[ss.jeu$date2 %in% jeu.ext$date2]))==nrow(ss.jeu))){
          # Pour NDWI de Gao, la moyenne de l'ensemble des données (sans la date) est toujours < à la moyenne à la date. Si la moyenne à la date et la moyenne de l'ensemble du jeu de données (sans la date) sont de même signe (+/-), alors on calcule le ratio moyenne de la date / moyenne de l'ensemble, sinon la moyenne de l'ensemble est négative, alors on calcule le ratio moyenne de la date / 0.0001.
          # -> Si le ratio > 5 et
          # -> Si l'écart-type à la date < à l'écart-type moyen (sans la date) avec un ratio > 10,
          # => Alors on décide qu'il y a un problème de détection de nuage.
          if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 2){
            if (jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j]) > rmoy.NDWIGAO & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDWIGAO) jeu0$pb.detec[jeu0$date2==j] = 1} else 
              if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 0){
                if (1/(jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j])) > rmoy.NDWIGAO & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDWIGAO) jeu0$pb.detec[jeu0$date2==j] = 1} else 
                  if (jeu$mean[jeu$date2==j]/0.0001 > rmoy.NDWIGAO & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.NDWIGAO) jeu0$pb.detec[jeu0$date2==j] = 1} else {
                    ## Cas où on a plusieurs valeurs dont des dates qui ne sont pas incluses dans la liste jeu.ext, on compare les données de la date étudiée à l'ensemble du sous-jeu de données
                    # Statistiques descriptives à partir de la moyenne
                    stat.moy = cbind(fid=unique(jeu0$fid),date2=j,val.obs=ss.jeu$mean[ss.jeu$date2==j],psych::describe(ss.jeu$mean))
                    # Statistiques descriptives à partir de l'écart-type (sans l'écart-type de la date)
                    stat.sd = cbind(data.frame(fid=unique(jeu0$fid),date2=j,val.obs=ss.jeu$std[ss.jeu$date2==j],mean=mean(ss.jeu$std[ss.jeu$date2!=j])))
                    # Si la moyenne est la valeur maximum du sous-jeu et si le ratio moyenne des écarts-type (sans la date)/écart-type de la date > 10, alors on décide qu'il y a un problème de détection de nuage (NB : fonnctionne aussi dans le cas où on a plus de date incluses dans jeu.text sur l'ensemble des dates)
                    if (stat.moy$val.obs != stat.moy$max & stat.sd$mean/stat.sd$val.obs > rsd.NDWIGAO){
                      if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 2){
                        if (jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j]) > rmoy.NDWIGAO) jeu0$pb.detec[jeu0$date2==j] = 1} else
                          if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 0){
                            if (1/(jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j])) > rmoy.NDWIGAO) jeu0$pb.detec[jeu0$date2==j] = 1} else
                              if (jeu$mean[jeu$date2==j]/0.0001 > rmoy.NDWIGAO) jeu0$pb.detec[jeu0$date2==j] = 1}
                    if (stat.moy$val.obs == stat.moy$max & stat.sd$mean/stat.sd$val.obs > rsd.NDWIGAO) jeu0$pb.detec[jeu0$date2==j] = 1
                  }}
      
      if (indice == "MNDWI"){
        ## Si on n'a qu'une valeur dans le sous-jeu de donnée ou qu'il y a plusieurs valeurs, mais qu'elles sont incluses dans la liste sélectionnée dans jeu.ext, alors on compare la valeur de cette date sur l'ensemble du jeu de données du polygone (moins la date) :
        if (nrow(ss.jeu) == 1 | (nlevels(factor(ss.jeu$date2[ss.jeu$date2 %in% jeu.ext$date2]))==nrow(ss.jeu))){
          # Pour NDWI de Gao, la moyenne de l'ensemble des données (sans la date) est toujours < à la moyenne à la date. Si la moyenne à la date et la moyenne de l'ensemble du jeu de données (sans la date) sont de même signe (+/-), alors on calcule le ratio moyenne de la date / moyenne de l'ensemble, sinon la moyenne de l'ensemble est négative, alors on calcule le ratio moyenne de la date / 0.0001.
          # -> Si le ratio > 5 et
          # -> Si l'écart-type à la date < à l'écart-type moyen (sans la date) avec un ratio > 10,
          # => Alors on décide qu'il y a un problème de détection de nuage.
          if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 2){
            if (jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j]) > rmoy.MNDWI & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.MNDWI) jeu0$pb.detec[jeu0$date2==j] = 1} else 
              if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 0){
                if (1/(jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j])) > rmoy.MNDWI & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.MNDWI) jeu0$pb.detec[jeu0$date2==j] = 1} else 
                  if (jeu$mean[jeu$date2==j]/0.0001 > rmoy.MNDWI & mean(jeu$std[jeu$date2!=j])/jeu$std[jeu$date2==j] > rsd.MNDWI) jeu0$pb.detec[jeu0$date2==j] = 1} else {
                    ## Cas où on a plusieurs valeurs dont des dates qui ne sont pas incluses dans la liste jeu.ext, on compare les données de la date étudiée à l'ensemble du sous-jeu de données
                    # Statistiques descriptives à partir de la moyenne
                    stat.moy = cbind(fid=unique(jeu0$fid),date2=j,val.obs=ss.jeu$mean[ss.jeu$date2==j],psych::describe(ss.jeu$mean))
                    # Statistiques descriptives à partir de l'écart-type (sans l'écart-type de la date)
                    stat.sd = cbind(data.frame(fid=unique(jeu0$fid),date2=j,val.obs=ss.jeu$std[ss.jeu$date2==j],mean=mean(ss.jeu$std[ss.jeu$date2!=j])))
                    # Si la moyenne est la valeur maximum du sous-jeu et si le ratio moyenne des écarts-type (sans la date)/écart-type de la date > 10, alors on décide qu'il y a un problème de détection de nuage (NB : fonnctionne aussi dans le cas où on a plus de date incluses dans jeu.text sur l'ensemble des dates)
                    if (stat.moy$val.obs != stat.moy$max & stat.sd$mean/stat.sd$val.obs > rsd.MNDWI){
                      if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 2){
                        if (jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j]) > rmoy.MNDWI) jeu0$pb.detec[jeu0$date2==j] = 1} else
                          if (sum(c(mean(jeu$mean[jeu$date2!=j]),jeu$mean[jeu$date2==j]) > 0) == 0){
                            if (1/(jeu$mean[jeu$date2==j]/mean(jeu$mean[jeu$date2!=j])) > rmoy.MNDWI) jeu0$pb.detec[jeu0$date2==j] = 1} else
                              if (jeu$mean[jeu$date2==j]/0.0001 > rmoy.MNDWI) jeu0$pb.detec[jeu0$date2==j] = 1}
                    if (stat.moy$val.obs == stat.moy$max & stat.sd$mean/stat.sd$val.obs > rsd.MNDWI) jeu0$pb.detec[jeu0$date2==j] = 1
                  }}
    }
    rm(jeu,bxp.moy,bxp.sd,jeu.ext,ss.jeu,stat.moy,stat.sd,j)
    return(jeu0)
  }
  
  tablo = lapply(tablo,function(x) detecloud(x,seuil.NA=seuil.NA))
  rm(detecloud)
  
  
  ################################################################################
  ################################################################################
  ########## Cas où on a plusieurs polygones et 3 ans ou plus de période observée ##########
  
  ### Si on a plus de 3 ans d'ancienneté, alors on exécute les 3 modèles
  tablo = mclapply(names(tablo),function(i){
    jeu = tablo[[i]]
    # Sélection des valeurs brutes qui seront conservées pour l'analyse
    jeu$raw.data = jeu$mean
    jeu$raw.data[jeu$pct.na > seuil.NA | jeu$pb.detect==1] = NA  # Suppression des valeurs avec une couverture nuageuse >75% et un écart-type très faible
    
    jeu1 = jeu[!is.na(jeu$raw.data),]  # Jeu complet pour cubic-spline
    
    ### 1er modèle : lissage par cubic spline (sans imputation des données)
    modele1 = smooth.spline(jeu1$date2,jeu1$raw.data,cv=TRUE)#,control.spar=list(low=-1,high=1))
    #qqnorm(residuals(modele1),main=i) # (à supprimer)
    #qqline(residuals(modele1),col=2) # (à supprimer)
    
    
    ### 2ème et 3ème modèle (si on a une période de 3 ans ou plus)
    if (diff.date >= 3*365){
      # Réaliser 10 sous-jeux de données
      set.seed(190382)
      echant = createFolds(1:nrow(jeu),k=5,returnTrain=TRUE)
      
      
      ### Calcul des seuils optimaux pour les modèles de Transformation de Fourier et B-spline pour chaque jeu d'apprentissage et vérification avec le jeu-test
      seuil.mod23 = t(data.frame(lapply(echant,function(x){
        # Création des jeux d'apprentissage et des jeux-test
        jeu.train = jeu[x,]
        jeu.test = jeu[-x,]
        
        ## Imputation du jeu de données si le pas de temps minimum vaut 5 jours
        if (pas.tps[i] == 5){
          # 1ère imputation : uniquement sur la période où on a des observations sur le polygone
          jours0 = data.frame(date2=seq(min(jeu.train$date2),max(jeu.train$date2),by=5))
          jeu0 = merge(jours0,jeu.train,by="date2",all.x=TRUE)
          jeu0 = jeu0[order(jeu0$date2),]
          jeu0$don.imp = jeu0$raw.data
          # Utilisation de l'imputation par moyenne-mobile (sauf pour MNDWI : moyenne)
          test = ts(jeu0$don.imp,frequency=73,start=c(year(min(jeu0$date2)),yday(min(jeu0$date2))/5))
          jeu0$don.imp = as.numeric(na_seadec(test,algorithm="ma",find_frequency=TRUE))
          # Surtout ne pas utiliser "mean" pour MNDWI car cela crée des saisonnalités quand il n'y en a pas !
          
          # 2ème imputation : sur toute la période étudiée
          jours1 = data.frame(date2=seq(min.date,max.date,by=5))
          jeu.train = merge(jours1,jeu0,by="date2",all.x=TRUE)
          jeu.train$fid[is.na(jeu.train$fid)] = i
          test = ts(jeu.train$don.imp,frequency=73,start=c(year(min(jeu.train$date2)),yday(min(jeu.train$date2))/5))
          # Utilisation de l'imputation par la moyenne sur la saisonnalité
          jeu.train$don.imp = as.numeric(na_seasplit(test,algorithm="mean",find_frequency=TRUE))
        } else {
          ## Imputation du jeu de données si le pas de temps minimum n'est pas 5
          # 1ère imputation : uniquement sur la période où on a des observations sur le polygone
          jours0 = data.frame(date2=seq(min(jeu.train$date2),max(jeu.train$date2),by=1))
          jeu0 = merge(jours0,jeu.train,by="date2",all.x=TRUE)
          jeu0 = jeu0[order(jeu0$date2),]
          jeu0$don.imp = jeu0$raw.data
          # Utilisation de l'imputation par moyenne-mobile (sauf pour MNDWI : moyenne)
          if (yday(min(jeu0$date2)) <= 365) test = ts(jeu0$don.imp,frequency=365,start=c(year(min(jeu0$date2)),yday(min(jeu0$date2)))) else
            test = ts(jeu0$don.imp,frequency=365,start=c(year(min(jeu0$date2)),365))
          jeu0$don.imp = as.numeric(na_seadec(test,algorithm="ma",find_frequency=TRUE))
          # Surtout ne pas utiliser "mean" pour MNDWI car cela crée des saisonnalités quand il n'y en a pas !
          
          # 2ème imputation : sur toute la période étudiée
          jours1 = data.frame(date2=seq(min.date,max.date,by=1))
          jeu.train = merge(jours1,jeu0,by="date2",all.x=TRUE)
          jeu.train$fid[is.na(jeu.train$fid)] = i
          if (yday(min(jeu.train$date2)) <= 365) test = ts(jeu.train$don.imp,frequency=365,start=c(year(min(jeu.train$date2)),yday(min(jeu.train$date2)))) else 
            test = ts(jeu.train$don.imp,frequency=365,start=c(year(min(jeu.train$date2)),365))
          # Utilisation de l'imputation par la moyenne sur la saisonnalité
          jeu.train$don.imp = as.numeric(na_seasplit(test,algorithm="mean",find_frequency=TRUE))
        }
        
        ### 2ème et 3ème modèles : lissage par transformation de Fourier et B-spline (avec imputation des données)
        # Etude sur un seuil d'ondelette compris entre 1 et 12% du jeu de données
        nb.seuil = unique(round(seq(1,12,by=1)*nrow(jeu.train)/100,digits=0))
        
        ## 2ème modèle : lissage par transformation de Fourier sur plusieurs seuils
        lissage.fourier = lapply(nb.seuil,function(seuil){
          if(odd(seuil) == FALSE) seuil = seuil + 1
          fct.fourier = create.fourier.basis(rangeval=range(as.numeric(jeu.train$date2)),nbasis=seuil)
          modele2 = smooth.basis(argvals=as.numeric(jeu.train$date2),y=jeu.train$don.imp,fdParobj=fct.fourier)$fd
          lissage2 = eval.fd(as.numeric(jeu.train$date2),modele2)
          data.frame(seuil=seuil,date2=jeu.train$date2,lissage2=lissage2)
        })
        names(lissage.fourier) = nb.seuil
        
        erreur.F = unlist(lapply(lissage.fourier,function(res.seuil){
          observe = jeu.test[!is.na(jeu.test$raw.data),c("date2","raw.data")]
          predit = res.seuil[res.seuil$date2 %in% jeu.test$date2[!is.na(jeu.test$raw.data)],c("date2","lissage2")]
          concat = merge(observe,predit,by="date2")
          rmse(concat$raw.data,concat$lissage2)
        }))
        # Définition du seuil optimal pour Fourier
        seuil.F = as.numeric(names(erreur.F)[erreur.F==min(erreur.F)])
        if (length(seuil.F) > 1) seuil.F = round(mean(seuil.F),digits=0)
        
        
        ## 3ème modèle : lissage par B-spline
        lissage.bspline = lapply(nb.seuil[nb.seuil >= 4 & nb.seuil < nrow(jeu.train)-4],function(seuil){
          fct.bspline = create.bspline.basis(rangeval=range(as.numeric(jeu.train$date2)),nbasis=seuil)
          modele3 = smooth.basis(argvals=as.numeric(jeu.train$date2),y=jeu.train$don.imp,fdParobj=fct.bspline)$fd
          lissage3 = eval.fd(as.numeric(jeu.train$date2),modele3)
          data.frame(seuil=seuil,date2=jeu.train$date2,lissage3=lissage3)
        })
        names(lissage.bspline) = nb.seuil[nb.seuil >= 4 & nb.seuil < nrow(jeu.train)-4]
        
        erreur.B = unlist(lapply(lissage.bspline,function(res.seuil){
          observe = jeu.test[!is.na(jeu.test$raw.data),c("date2","raw.data")]
          predit = res.seuil[res.seuil$date2 %in% jeu.test$date2[!is.na(jeu.test$raw.data)],c("date2","lissage3")]
          concat = merge(observe,predit,by="date2")
          rmse(concat$raw.data,concat$lissage3)
        }))
        # Définition du seuil optimal pour Fourier
        seuil.B = as.numeric(names(erreur.B)[erreur.B==min(erreur.B)])
        if (length(seuil.B) > 1) seuil.B = round(mean(seuil.B),digits=0)
        
        
        seuil.opt = c(seuil.F*100/nrow(jeu.train),seuil.B*100/nrow(jeu.train))
        names(seuil.opt) = c("Fourier","B.spline")
        seuil.opt 
      })))
      
      
      ## Imputation du jeu de données si le pas de temps minimum vaut 5 jours
      if (pas.tps[i] == 5){
        # 1ère imputation : uniquement sur la période où on a des observations sur le polygone
        jours0 = data.frame(date2=seq(min(jeu$date2),max(jeu$date2),by=5))
        jeu0 = merge(jours0,jeu,by="date2",all.x=TRUE)
        jeu0 = jeu0[order(jeu0$date2),]
        jeu0$don.imp = jeu0$raw.data
        # Utilisation de l'imputation par moyenne-mobile (sauf pour MNDWI : moyenne)
        test = ts(jeu0$don.imp,frequency=73,start=c(year(min(jeu0$date2)),yday(min(jeu0$date2))/5))
        jeu0$don.imp = as.numeric(na_seadec(test,algorithm="ma",find_frequency=TRUE))
        # Surtout ne pas utiliser "mean" pour MNDWI car cela crée des saisonnalités quand il n'y en a pas !
        
        # 2ème imputation : sur toute la période étudiée
        jours1 = data.frame(date2=seq(min.date,max.date,by=5))
        jeu = merge(jours1,jeu0,by="date2",all.x=TRUE)
        jeu$fid[is.na(jeu$fid)] = i
        test = ts(jeu$don.imp,frequency=73,start=c(year(min(jeu$date2)),yday(min(jeu$date2))/5))
        # Utilisation de l'imputation par la moyenne sur la saisonnalité
        jeu$don.imp = as.numeric(na_seasplit(test,algorithm="mean",find_frequency=TRUE))
      } else {
        ## Imputation du jeu de données si le pas de temps minimum n'est pas 5
        # 1ère imputation : uniquement sur la période où on a des observations sur le polygone
        jours0 = data.frame(date2=seq(min(jeu$date2),max(jeu$date2),by=1))
        jeu0 = merge(jours0,jeu,by="date2",all.x=TRUE)
        jeu0 = jeu0[order(jeu0$date2),]
        jeu0$don.imp = jeu0$raw.data
        # Utilisation de l'imputation par moyenne-mobile (sauf pour MNDWI : moyenne)
        if (yday(min(jeu0$date2)) <= 365) test = ts(jeu0$don.imp,frequency=365,start=c(year(min(jeu0$date2)),yday(min(jeu0$date2)))) else
          test = ts(jeu0$don.imp,frequency=365,start=c(year(min(jeu0$date2)),365))
        jeu0$don.imp = as.numeric(na_seadec(test,algorithm="ma",find_frequency=TRUE))
        # Surtout ne pas utiliser "mean" pour MNDWI car cela crée des saisonnalités quand il n'y en a pas !
        
        # 2ème imputation : sur toute la période étudiée
        jours1 = data.frame(date2=seq(min.date,max.date,by=1))
        jeu = merge(jours1,jeu0,by="date2",all.x=TRUE)
        jeu$fid[is.na(jeu$fid)] = i
        if (yday(min(jeu$date2)) <= 365) test = ts(jeu$don.imp,frequency=365,start=c(year(min(jeu$date2)),yday(min(jeu$date2)))) else 
          test = ts(jeu$don.imp,frequency=365,start=c(year(min(jeu$date2)),365))
        # Utilisation de l'imputation par la moyenne sur la saisonnalité
        jeu$don.imp = as.numeric(na_seasplit(test,algorithm="mean",find_frequency=TRUE))
      }
      
      
      # Calcul de la moyenne de chaque seuil par groupe de jeux
      seuil.mod23 = apply(seuil.mod23,2,function(x) round(mean(x)*nrow(jeu)/100,digits=0))
      
      ## 2ème modèle : lissage par transformation de Fourier
      # Exécution du modèle au seuil optimal
      if(odd(seuil.mod23["Fourier"]) == FALSE) seuil.mod23["Fourier"] = seuil.mod23["Fourier"] + 1
      fct.fourier = create.fourier.basis(rangeval=range(as.numeric(jeu$date2)),nbasis=seuil.mod23["Fourier"])
      modele2 = smooth.basis(argvals=as.numeric(jeu$date2),y=jeu$don.imp,fdParobj=fct.fourier)$fd
      seuil.F = seuil.mod23["Fourier"]*100/nrow(jeu)
      
      
      ## 3ème modèle : lissage par B-spline
      # Exécution du modèle au seuil optimal  
      if (seuil.mod23["B.spline"] < 4) seuil.mod23["B.spline"] = 4
      fct.bspline = create.bspline.basis(rangeval=range(as.numeric(jeu$date2)),nbasis=seuil.mod23["B.spline"])
      modele3 = smooth.basis(argvals=as.numeric(jeu$date2),y=jeu$don.imp,fdParobj=fct.bspline)$fd
      seuil.B = seuil.mod23["B.spline"]*100/nrow(jeu)
    }
    
    
    ### Lissage sur toutes les dates de la période totale pour les 3 modèles
    jours = seq(min(donnees$date2),max(donnees$date2),by=1)
    
    lissage1 = predict(modele1,as.numeric(jours))$y
    if (diff.date >= 3*365){
      lissage2 = as.numeric(eval.fd(as.numeric(jours),modele2))
      lissage3 = as.numeric(eval.fd(as.numeric(jours),modele3))
    }
    
    ### Insertion dans le jeu de données final
    if (diff.date >= 3*365)
      resultat = data.frame(index=indice,fid=i,date2=jours,cubic.spline=lissage1,fourier=lissage2,seuil.F=as.numeric(seuil.F),b.spline=lissage3,seuil.B=as.numeric(seuil.B),stringsAsFactors=TRUE) else resultat = data.frame(index=indice,fid=i,date2=jours,cubic.spline=lissage1,stringsAsFactors=TRUE)
    # On récupère les autres variables du jeu de données
    jeu = merge(resultat,jeu,by=c("fid","date2"),all.x=TRUE)
    # Rajout des variables du jeu d'origine à partir de fid
    jeu = merge(jeu,jeu.orig,by="fid",all.x=TRUE)

    #cat(i, "\n")
    return(jeu)
  }
  ,mc.cores=nb.coeurs)
  rm(pas.tps)
  
  ### Concaténation de la liste en data.frame
  sortie = NULL
  for (i in 1:length(tablo)) sortie = rbind(sortie,tablo[[i]])
  
  
  #~ ################################################################################
  #~ ################################################################################
  #~ ### Représentation graphique des 3 types de lissage
  #~ if (diff.date >= 3*365) pdf(paste("./Graphique ",indice," par polygone avec 3 méthodes de lissage (seuil optimal).pdf",sep=""),width=12,height=8) else pdf(paste("./Graphique ",indice," par polygone avec lissage Cubic Spline.pdf",sep=""),width=12,height=8)
  #~ if (nlevels(donnees$fid)>1) layout(matrix(1:6,nrow=3,ncol=2,byrow=TRUE)) else layout(matrix(1:2,nrow=2,ncol=1,byrow=TRUE))
  #~ for (i in levels(sortie$fid)){
  #~   plot(mean~date2,data=sortie,subset=(fid==i),main=paste(indice,i,sep=" - "),las=1)
  #~   points(mean~date2,data=sortie,subset=(fid==i & pct.na>75),col=2,pch=16)
  #~   points(mean~date2,data=sortie,subset=(fid==i & pb.detec==1),col=3,pch=16)
  #~   lines(cubic.spline~date2,data=sortie,subset=(fid==i),col=2)
  #~   if (diff.date >= 3*365){
  #~     lines(fourier~date2,data=sortie,subset=(fid==i),col=4)
  #~     lines(b.spline~date2,data=sortie,subset=(fid==i),col=6)
  #~   }
  #~ }
  #~ plot(0,type="n",xaxt="n",yaxt="n",bty="n",xlab="",ylab="")
  #~ if (diff.date >= 3*365) legend("center",legend=c(paste("Raw data",c("included in the model",paste("with > ",seuil.NA,"% NA",sep=""),paste("<= ",seuil.NA,"% NA with detection cloud's problem",sep="")),sep=" "),paste("Fitted data : ",c("cubic spline","Fourier transformation","B-spline"),sep="")),title="Legend",col=c(1:3,2,4,6),lty=c(rep(NA,3),rep(1,3)),pch=c(1,rep(16,2),rep(NA,3))) else 
  #~   legend("center",legend=c(paste("Raw data",c("included in the model",paste("with > ",seuil.NA,"% NA",sep=""),paste("<= ",seuil.NA,"% NA with detection cloud's problem",sep="")),sep=" "),"Fitted data : cubic spline"),title="Legend",col=c(1:3,2),lty=c(rep(NA,3),1),pch=c(1,rep(16,2),NA))
  #~ dev.off()
  
  
  #~ ### Calcul du RMSE par modèle (sur l'ensemble des données observées) ###
  #~ if (diff.date >= 3*365){
  #~   RMSE.mod = NULL
  #~   for (i in levels(sortie$fid)){
  #~     rmse.cubic = rmse(sortie$cubic.spline[sortie$fid==i & !is.na(sortie$raw.data)],sortie$raw.data[sortie$fid==i & !is.na(sortie$raw.data)])
  #~     rmse.fourier = rmse(sortie$fourier[sortie$fid==i & !is.na(sortie$raw.data)],sortie$raw.data[sortie$fid==i & !is.na(sortie$raw.data)])
  #~     rmse.bspline = rmse(sortie$b.spline[sortie$fid==i & !is.na(sortie$raw.data)],sortie$raw.data[sortie$fid==i & !is.na(sortie$raw.data)])
  #~     resultat = data.frame(fid=i,rmse.cubic=rmse.cubic,rmse.fourier=rmse.fourier,rmse.bspline=rmse.bspline)
  #~     resultat$best.mod = ""
  #~     if(resultat$rmse.cubic == min(resultat[-match(c("fid","best.mod"),names(resultat))])) resultat$best.mod = "Cubic" else
  #~       if(resultat$rmse.fourier == min(resultat[-match(c("fid","best.mod"),names(resultat))])) resultat$best.mod = "Fourier" else
  #~         resultat$best.mod = "B-spline"
  #~     RMSE.mod = rbind(RMSE.mod,resultat)
  #~     rm(resultat,rmse.cubic,rmse.fourier,rmse.bspline)
  #~     # Exportation si besoin
  #~     write.csv2(sortie,paste("./Lissage ",indice," - RMSE par modèle.csv",sep=""),row.names=FALSE)
  
  #~   }
  
  #~   # Représentation graphique du lissage optimal
  #~   pdf(paste("./Graphique ",indice," par polygone avec le lissage optimal.pdf",sep=""),width=12,height=8)
  #~   if (nlevels(donnees$fid)>1) layout(matrix(1:6,nrow=3,ncol=2,byrow=TRUE)) else layout(matrix(1:2,nrow=2,ncol=1,byrow=TRUE))
  #~   for (i in levels(sortie$fid)){
  #~     plot(mean~date2,data=sortie,subset=(fid==i),main=paste(indice,i,sep=" - "),las=1)
  #~     points(mean~date2,data=sortie,subset=(fid==i & pct.na>75),col=2,pch=16)
  #~     points(mean~date2,data=sortie,subset=(fid==i & pb.detec==1),col=3,pch=16)
  #~     if (RMSE.mod$best.mod[RMSE.mod$fid==i]=="Cubic") lines(cubic.spline~date2,data=sortie,subset=(fid==i),col=2) else
  #~       if (RMSE.mod$best.mod[RMSE.mod$fid==i]=="Fourier") lines(fourier~date2,data=sortie,subset=(fid==i),col=4) else
  #~         if (RMSE.mod$best.mod[RMSE.mod$fid==i]=="B-spline") lines(b.spline~date2,data=sortie,subset=(fid==i),col=6)
  #~   }
  #~   plot(0,type="n",xaxt="n",yaxt="n",bty="n",xlab="",ylab="")
  #~   legend("center",legend=c(paste("Raw data",c("included in the model",paste("with > ",seuil.NA,"% NA",sep=""),paste("<= ",seuil.NA,"% NA with detection cloud's problem",sep="")),sep=" "),paste("Fitted data : ",c("cubic spline","Fourier transformation","B-spline"),sep="")),title="Legend",col=c(1:3,2,4,6),lty=c(rep(NA,3),rep(1,3)),pch=c(1,rep(16,2),rep(NA,3)))
  #~   dev.off()
  #~ }
  
  
  
  
  
  
  
  
  
  ################################################################################
  # Sauvegarde des bases de données des indices d'environnement
  write.csv2(sortie,paste(substring(nom.fichier,1,(nchar(nom.fichier)-4)),"_smooth.csv",sep=""),row.names=FALSE)
  
  #saveRDS(sortie,paste("./Lissage_",indice,"_",format(min.date,format="%Y%m%d"),"_",format(max.date,format="%Y%m%d"),".rds",sep=""))
  
  ################################################################################
  
  # Suppression de tous les objets
  rm(list=ls())
}
