# Modules
library(kernlab)
library(parallel)
library(plyr)


# R�pertoire du travail
setwd("/home/webdev/shiny/appli/sen2extract/www/extractions/yangoon/")

nb.coeurs = detectCores()


# Importation du mod�le sur 500m
modele = readRDS("Modele_de_prediction_SVM_500m.rds")

# Param�tres de d�calage sur 500m pour chaque indice
#"NDVI.s2","NDWIGAO.s11","MNDWI.s14"



#########################################################################################
########## PREDICTION SUR LES INDICES D'ENVIRONNEMENT LISSES SUR LES HEXAGONES ##########
#########################################################################################

# Importation des indices d'environnement sur l'hexagone
NDVI = readRDS("Yangon_hexasurv_named_NDVI_smooth_current.rds")
NDWIGAO = readRDS("Yangon_hexasurv_named_NDWIGAO_smooth_current.rds")
MNDWI = readRDS("Yangon_hexasurv_named_MNDWI_smooth_current.rds")


# On ne conserve que les variables utiles
NDVI = NDVI[,c("fid","date2","fourier","id","name")]
NDWIGAO = NDWIGAO[,c("fid","date2","fourier","id","name")]
MNDWI = MNDWI[,c("fid","date2","fourier","id","name")]

# Triage en fonction des polygone et des dates
NDVI = NDVI[order(NDVI$fid,NDVI$date2),]
NDWIGAO = NDWIGAO[order(NDWIGAO$fid,NDWIGAO$date2),]
MNDWI = MNDWI[order(MNDWI$fid,MNDWI$date2),]

# Concatenation des 3 jeux de donnees en prenant le decalage voulu pour chaque polygone
periode = sort(unique(NDVI$date2))
prevision = mclapply(levels(NDVI$fid),function(i){
  
  ### Mise en forme des 3 jeux de donnees ###
  ## NDVI avec un decalage de 2 semaines (2 fois 5 jours)
  dNDVI = NDVI[NDVI$fid==i & NDVI$date2 %in% periode[1:(length(periode)-2)],]
  # Ajout de la variable periode
  dNDVI$periode = periode[(2+1):length(periode)]
  # Suppression de la variables date2
  dNDVI = dNDVI[,-match("date2",names(dNDVI))]
  # Renommage de variable "fourier"
  names(dNDVI)[match("fourier",names(dNDVI))] = "NDVI"
  
  
  ## NDWIGAO avec un decalage de 11 semaines (11 fois 5 jours)
  dNDWIGAO = NDWIGAO[NDWIGAO$fid==i & NDWIGAO$date2 %in% periode[1:(length(periode)-11)],]
  # Ajout de la variable periode
  dNDWIGAO$periode = periode[(11+1):length(periode)]
  # Suppression de la variables date2
  dNDWIGAO = dNDWIGAO[,-match("date2",names(dNDWIGAO))]
  # Renommage de variable "fourier"
  names(dNDWIGAO)[match("fourier",names(dNDWIGAO))] = "NDWIGAO"
  
  # MNDWI avec un decalage de 14 semaines (14 fois 5 jours)
  dMNDWI = MNDWI[MNDWI$fid==i & MNDWI$date2 %in% periode[1:(length(periode)-14)],]
  # Ajout de la variable periode
  dMNDWI$periode = periode[(14+1):length(periode)]
  # Suppression de la variables date2
  dMNDWI = dMNDWI[,-match("date2",names(dMNDWI))]
  # Renommage de variable "fourier"
  names(dMNDWI)[match("fourier",names(dMNDWI))] = "MNDWI"
  
  ## Concatenation des 3 jeux de donn�es
  jeu = merge(dNDVI,dNDWIGAO,by=c("fid","id","name","periode"))
  jeu = merge(jeu,dMNDWI,by=c("fid","id","name","periode"))

  # Prediction sur l'ensemble des donnees a toutes les periodes
  jeu$tx.posit = predict(modele,newdata=jeu,type="prob")$Positive
  
  jeu
  },mc.cores=nb.coeurs)

# Transformation de la liste en format data.frame
sortie = ldply(prevision, data.frame)

# Triage en fonction de la date
sortie = sortie[order(sortie$periode,sortie$fid),]

# Exportation du fichier
saveRDS(sortie,"Yangon_hexasurv_named_predict_current.rds")



#########################################################################################
########## PREDICTION SUR LES INDICES D'ENVIRONNEMENT LISSES SUR LES SEGMENTS ##########
#########################################################################################

rm(list=ls()[-match(c("modele","nb.coeurs"),ls())])

# Importation des indices d'environnement sur l'hexagone
NDVI = readRDS("Yangon_v5_shape08_compact1_scale300_inter_hexa_WGS84_NDVI_smooth_current.rds")
NDWIGAO = readRDS("Yangon_v5_shape08_compact1_scale300_inter_hexa_WGS84_NDWIGAO_smooth_current.rds")
MNDWI = readRDS("Yangon_v5_shape08_compact1_scale300_inter_hexa_WGS84_MNDWI_smooth_current.rds")

# On ne conserve que les variables utiles
NDVI = NDVI[,c("fid","date2","fourier","ID","name")]
NDWIGAO = NDWIGAO[,c("fid","date2","fourier","ID","name")]
MNDWI = MNDWI[,c("fid","date2","fourier","ID","name")]

# Triage en fonction des polygone et des dates
NDVI = NDVI[order(NDVI$fid,NDVI$date2),]
NDWIGAO = NDWIGAO[order(NDWIGAO$fid,NDWIGAO$date2),]
MNDWI = MNDWI[order(MNDWI$fid,MNDWI$date2),]

# Concatenation des 3 jeux de donnees en prenant le decalage voulu pour chaque polygone
periode = sort(unique(NDVI$date2))
prevision = mclapply(levels(NDVI$fid),function(i){
  
  ### Mise en forme des 3 jeux de donnees ###
  ## NDVI avec un decalage de 2 semaines (2 fois 5 jours)
  dNDVI = NDVI[NDVI$fid==i & NDVI$date2 %in% periode[1:(length(periode)-2)],]
  # Ajout de la variable periode
  dNDVI$periode = periode[(2+1):length(periode)]
  # Suppression de la variables date2
  dNDVI = dNDVI[,-match("date2",names(dNDVI))]
  # Renommage de variable "fourier"
  names(dNDVI)[match("fourier",names(dNDVI))] = "NDVI"
  
  
  ## NDWIGAO avec un decalage de 11 semaines (11 fois 5 jours)
  dNDWIGAO = NDWIGAO[NDWIGAO$fid==i & NDWIGAO$date2 %in% periode[1:(length(periode)-11)],]
  # Ajout de la variable periode
  dNDWIGAO$periode = periode[(11+1):length(periode)]
  # Suppression de la variables date2
  dNDWIGAO = dNDWIGAO[,-match("date2",names(dNDWIGAO))]
  # Renommage de variable "fourier"
  names(dNDWIGAO)[match("fourier",names(dNDWIGAO))] = "NDWIGAO"
  
  # MNDWI avec un decalage de 14 semaines (14 fois 5 jours)
  dMNDWI = MNDWI[MNDWI$fid==i & MNDWI$date2 %in% periode[1:(length(periode)-14)],]
  # Ajout de la variable periode
  dMNDWI$periode = periode[(14+1):length(periode)]
  # Suppression de la variables date2
  dMNDWI = dMNDWI[,-match("date2",names(dMNDWI))]
  # Renommage de variable "fourier"
  names(dMNDWI)[match("fourier",names(dMNDWI))] = "MNDWI"
  
  ## Concatenation des 3 jeux de donnees
  jeu = merge(dNDVI,dNDWIGAO,by=c("fid","ID","name","periode"))
  jeu = merge(jeu,dMNDWI,by=c("fid","ID","name","periode"))
  
  # Prediction sur l'ensemble des donnees � toutes les periodes
  jeu$tx.posit = predict(modele,newdata=jeu,type="prob")$Positive
  
  jeu
},mc.cores=nb.coeurs)

# Transformation de la liste en format data.frame
sortie = ldply(prevision, data.frame)

# Triage en fonction de la date
sortie = sortie[order(sortie$periode,sortie$fid),]

# Exportation du fichier
saveRDS(sortie,"Yangon_v5_shape08_compact1_scale300_inter_hexa_WGS84_predict_current.rds")

