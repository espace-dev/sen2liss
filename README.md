Sen2Liss
===========

<!-- badges: start -->

<!-- badges: end -->


Sen2Liss gathers R scripts to perform a temporal modeling of spectral indices computed from Sentinel-2 satellite images (for instance: NDVI, MNDWI, NDWI Gao). Each script has been adapted for a specific context. The use of Sen2Liss comes after the use of Sen2Extract, i.e. once the values of the spectral indices have been calculated on a set of polygons, for a given period. The use of Sen2Liss aims to calculate missing values (when there are clouds) and to detect outliers (which have not been detected as clouds). Sen2Liss also allows to compute modeled values at any date in the study period.

Sen2Liss is not an operational script for any site or any date. It should be adapted by those interested. Nevertheless, we are interested in feedback from future users and improvements that could be made.


These scripts were initially developed by UMR ESPACE-DEV (IRD, Univ Antilles, Univ Guyane, Univ Montpellier, Univ Réunion) at University of La Réunion – SEAS-OI Station, in the frame of the S2-Malaria Project. This project, funded by CNES (TOSCA 2017-2020), aims at using satellite data provided by Sentinel-2 for epidemiological surveillance (intially for malaria). The main objective is to guarantee quick and easy access to these images and the resulting indices.

The original Sen2Extract application is available here: [Sen2Extract SEAS-OI](http://indices.seas-oi.org/sen2extract/).
Sen2Extract does not allow to extract indices from any locality in the globe, but only on the Sentinel-2 L2A tiles available at SEAS-OI that have been produced in the framework of different projects.
See the About section for more information.



Contributing
------------

Scientific projects :

*  TOSCA S2-Malaria project, funded by CNES (TOSCA 2017-2020); 
*  INTERREG Renovrisk-impact project (2018-2020).

Development and improvment :

*  Sylvaine Jégo ;
*  Pascal Mouquet.


![Espace-Dev](../docs/source/logos_espacedev_ird_ur.png)


This project is open to anyone willing to help. You can contribute by adding new features, reporting and fixing bugs, or propose new ideas.

Funding
-------

![Funded by CNES and INTERREG V](../docs/source/logos_cnes_interreg_ird.png)

License
-------

GPLv3+
